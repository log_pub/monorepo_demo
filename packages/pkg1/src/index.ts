import { pkg2_fun } from "pkg2";
function fun() {
  const msg: string = "我是msg";
  console.log("I am package 1-");
  pkg2_fun();
  return msg;
}

export default fun;
