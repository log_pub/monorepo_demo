import React, { lazy, Suspense, useState } from "react";
import fun from "pkg1";

import smallImg from "./assets/imgs/5kb.png";
import bigImg from "./assets/imgs/22kb.png";

// prefetch
const PreFetchDemo = lazy(
  () =>
    import(
      /* webpackChunkName: "PreFetchDemo" */
      /*webpackPrefetch: true*/
      "@/components/PreFetchDemo"
    ),
);
// preload
const PreloadDemo = lazy(
  () =>
    import(
      /* webpackChunkName: "PreloadDemo" */
      /*webpackPreload: true*/
      "@/components/PreloadDemo"
    ),
);

import "./app.css";
import "./app.less";

function App() {
  const [count, setCounts] = useState("");
  const [show, setShow] = useState(false);
  const onChange = (e: any) => {
    setCounts(e.target.value);
  };

  const onClick = () => {
    setShow(true);
    fun();
  };
  return (
    <>
      <button onClick={onClick}>展示</button>
      {show && (
        <>
          <Suspense fallback={null}>
            <PreloadDemo />
          </Suspense>
          <Suspense fallback={null}>
            <PreFetchDemo />
          </Suspense>
        </>
      )}
    </>
  );
}

export default App;
